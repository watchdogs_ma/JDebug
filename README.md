# JDebug
Visual Debugger For The Jolt Physics Engine, Similar to PhysX PVD.

# Warnings

Very Experimental, Not Ready For Production.

# Why Create A "Visual Debugger"?

Well, I Have 3 Core Reasons:

1: Jolt Doesn't have a Fleshed debugger. Despite saving binary "Playbacks" of the physics world, it isn't very verbose when compared other debuggers like Havok's Debugger.

2: I Want/Wanted to learn more about the physics engine.  I have tinkered around with
other physics engines(PhysX, Bullet3,Havok) but never really liked how the two were not very extendable.  

# Building 

The project uses cmake to generate. 

I Heavily recommend installing the Gui, and configuring the project through there.

Windows Is The Only Platform I've Tested & Developed The Tool On. Linux Support Is Planned.

